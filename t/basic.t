use Test;
plan test => 8;
eval { require Devel::Leak };
ok($@, "", "loading module");
eval { import Devel::Leak };
ok($@, "", "running import");

eval { Devel::Leak::CheckSV($handle) };
ok($@ =~ /\QPlease call Devel::Check::NoteSV() before calling Devel::Check::CheckSV()/, 1, "no segfault");

@somewhere = ();
my $count = Devel::Leak::NoteSV($handle);
print "$count SVs so far\n";
for my $i (1..10)
 {
  @somewhere = qw(one two);
 }
my $now = Devel::Leak::CheckSV($handle);
ok($now, $count+2, "Number of SVs created unexpected");

eval { Devel::Leak::CheckSV($handle) };
ok($@ =~ /\QPlease call Devel::Check::NoteSV() before calling Devel::Check::CheckSV()/, 1, "cannot call CheckSV() twice");

{
 my @somewhere_else = ();
 my $handle2;
 my $count = Devel::Leak::NoteSV($handle2);
 print "$count SVs so far\n";
 for my $i (1..10)
  {
   @somewhere_else = qw(one two);
  }
 my $sv_list = Devel::Leak::ListSV($handle2);
 ok($sv_list->[0], "one", "First leaked SV");
 ok($sv_list->[1], "two", "Second leaked SV");
 ok(@$sv_list, 2, "Count of leaked SVs");
}
